﻿using UnityEngine;
using System.Collections;

public class fırlat2 : MonoBehaviour {

   public Rigidbody nesne;
    public AudioClip firlatmasesi;
    public AudioClip reloaded;
    int count = 5;
    bool reloading = true;
 
	
	// Update is called once per frame
	void Update () {
        if ((Input.GetMouseButtonDown(0))&&(reloading==true))
        {
            Rigidbody firlatilan;
            firlatilan = Instantiate(nesne, GameObject.Find("Shooter1").transform.position, Quaternion.identity) as Rigidbody;
            firlatilan.velocity = transform.TransformDirection(Vector3.forward * 25);
            firlatilan.name = "shell";
            GetComponent<AudioSource>().PlayOneShot(firlatmasesi);
            count--;

        }
        if (Input.GetKeyDown(KeyCode.R))
            reload();

        if (count <= 0)
        {
            reloading = false;
           
           
        }
	
	}
    void reload()
    {
        count = 5;
        reloading = true;
        GetComponent<AudioSource>().PlayOneShot(reloaded);

    }
}
