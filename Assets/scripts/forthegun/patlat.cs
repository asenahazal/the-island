﻿using UnityEngine;
using System.Collections;

public class patlat : MonoBehaviour {
    public AudioClip hedefevurma;
    public AudioClip iska;
    public GameObject yoket;
	// Use this for initialization
	void Start () {
        Destroy(gameObject, 5);
	}
	
	// Update is called once per frame
    void OnTriggerEnter(Collider vurdu)
    {
        GetComponent<AudioSource>().PlayOneShot(iska);
        if (vurdu.gameObject.tag == "hedef")
        {
            GetComponent<AudioSource>().PlayOneShot(hedefevurma);
            Destroy(yoket);
        }
    }
}
