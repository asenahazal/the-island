﻿using UnityEngine;
using System.Collections;

public class firlat : MonoBehaviour {
    public Rigidbody nesne;
    public AudioClip firlatmasesi;
    public AudioClip reloaded;
    int count = 7;
    bool reloading = true;
 
	
	// Update is called once per frame
	void Update () {
        if ((Input.GetMouseButtonDown(0))&&(reloading==true))
        {
            Rigidbody firlatilan;
            firlatilan = Instantiate(nesne, GameObject.Find("Shooter").transform.position, Quaternion.identity) as Rigidbody;
            firlatilan.velocity = transform.TransformDirection(Vector3.forward * 15);
            firlatilan.name = "shell";
            GetComponent<AudioSource>().PlayOneShot(firlatmasesi);
            count--;

        }
        if (Input.GetKeyDown(KeyCode.R))
            reload();

        if (count <= 0)
        {
            reloading = false;
           
           
        }
	
	}
    void reload()
    {
        count = 7;
        reloading = true;
        GetComponent<AudioSource>().PlayOneShot(reloaded);

    }
}
