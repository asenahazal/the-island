﻿using UnityEngine;
using System.Collections;

public class attackwithsword : MonoBehaviour
{

    public AudioClip slide;
    Animator sword;
    void Awake()
    {
        sword = GetComponent<Animator>();
    }

    void OnTriggerStay(Collider geldi)
    {
        if ((geldi.gameObject.name == "Capsule") && Input.GetMouseButtonDown(0))
        {
            sword.SetBool("attack", true);
            GetComponent<AudioSource>().PlayOneShot(slide);
        }

        else if ((geldi.gameObject.name == "Capsule") && Input.GetMouseButtonUp(0))
        {
            sword.SetBool("attack", false);
        }
    }
}